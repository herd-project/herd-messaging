package herdmsg

import (
	"context"
	"fmt"
	"io"

	"cloud.google.com/go/pubsub"

	"gitlab.com/herd-project/herd-shared/herdlog"
)

type MessageHandler interface {
	HandleModelUpdated(ctx context.Context, msg ModelDidUpdateMsg) error
	HandleDirectMessage(ctx context.Context, msg DirectMessage) error
}

type Receiver interface {
	io.Closer
	Run(context.Context, MessageHandler) error
}

func NewReceiver(ctx context.Context, projectID string, subID string, dlqTopicID string) (Receiver, error) {
	client, err := pubsub.NewClient(ctx, projectID)
	if err != nil {
		return nil, err
	}

	sub := client.Subscription(subID)
	dlqTopic := client.Topic(dlqTopicID)

	return &receiverImpl{
		client: client,
		sub:    sub,
		dlq:    dlqTopic,
	}, nil
}

// implementation

type receiverImpl struct {
	client *pubsub.Client
	sub    *pubsub.Subscription
	dlq    *pubsub.Topic
}

func (r *receiverImpl) Run(ctx context.Context, handler MessageHandler) error {
	go func() {
		err := r.sub.Receive(ctx, func(ctx context.Context, msg *pubsub.Message) {
			defer msg.Ack()

			log := herdlog.FromContext(ctx).WithFields(map[string]interface{}{"message-id": msg.ID, "attributes": msg.Attributes})
			log.Info("received message")

			var err error

			msgType := msg.Attributes[MsgTypeKey]
			switch msgType {
			case MsgTypeModelDidUpdate:
				err = r.handleModelDidUpdate(ctx, msg, handler)
			case MsgTypeDirectMessage:
				err = r.handleDirectUsersMsg(ctx, msg, handler)
			default:
				err = fmt.Errorf("unknown message type: %s", msgType)
			}

			if err != nil {
				log.WithError(err).Error("receiver error")
				r.dlq.Publish(ctx, msg)
			}

			log.Info("message handled")
		})

		if err != nil {
			herdlog.FromContext(ctx).WithError(err).Panic("pub/sub subscription Receive error")
		}
	}()

	return nil
}

func (r *receiverImpl) Close() error {
	if r.client != nil {
		return r.client.Close()
	}
	return nil
}

func (r *receiverImpl) handleModelDidUpdate(ctx context.Context, msg *pubsub.Message, handler MessageHandler) error {
	obj, err := UnmarshalModelUpdatedMessage(msg.Data)
	if err != nil {
		return fmt.Errorf("unmarshal error: %w", err)
	}

	err = handler.HandleModelUpdated(ctx, obj)
	if err != nil {
		return fmt.Errorf("handler error: %w", err)
	}

	return nil
}

func (r *receiverImpl) handleDirectUsersMsg(ctx context.Context, msg *pubsub.Message, handler MessageHandler) error {
	obj, err := UnmarshalDirectMessage(msg.Data)
	if err != nil {
		return fmt.Errorf("unmarshal error: %w", err)
	}

	err = handler.HandleDirectMessage(ctx, obj)
	if err != nil {
		return fmt.Errorf("handler error: %w", err)
	}

	return nil
}
