package herdmsg

import (
	"encoding/json"
)

const (
	MsgTypeKey            = "message-type"
	MsgTypeModelDidUpdate = "model-did-update"
	MsgTypeDirectMessage  = "direct-message"
)

// ModelDidUpdateMsg

const (
	ProfileDidUpdateMsgType       = iota + 1
	ProfileAvatarDidUpdateMsgType

	UserDidJoinEventMsgType
	UserDidLeaveEventMsgType
	UserDidCloseEventMsgType

	EventDidCreateMsgType

	EventLocationDidUpdateMsgType
	EventScheduleDidUpdateMsgType

	EventChatDidUpdateMsgType
)

type ModelDidUpdateMsg struct {
	MsgType int    `json:"msg_type"`
	UserID  string `json:"user_id"`
	EventID string `json:"event_id,omitempty"`
}

func NewProfileDidUpdateMsg(userID string) ModelDidUpdateMsg {
	return ModelDidUpdateMsg{
		MsgType: ProfileDidUpdateMsgType,
		UserID:  userID,
	}
}

func NewProfileAvatarDidUpdateMsg(userID string) ModelDidUpdateMsg {
	return ModelDidUpdateMsg{
		MsgType: ProfileAvatarDidUpdateMsgType,
		UserID:  userID,
	}
}

func NewUserDidJoinEventMsg(userID string, eventID string) ModelDidUpdateMsg {
	return ModelDidUpdateMsg{
		MsgType: UserDidJoinEventMsgType,
		UserID:  userID,
		EventID: eventID,
	}
}

func NewUserDidLeaveEventMsg(userID string, eventID string) ModelDidUpdateMsg {
	return ModelDidUpdateMsg{
		MsgType: UserDidLeaveEventMsgType,
		UserID:  userID,
		EventID: eventID,
	}
}

func NewUserDidCloseEventMsg(userID string, eventID string) ModelDidUpdateMsg {
	return ModelDidUpdateMsg{
		MsgType: UserDidCloseEventMsgType,
		UserID:  userID,
		EventID: eventID,
	}
}

func NewEventDidCreateMsg(userID string, eventID string) ModelDidUpdateMsg {
	return ModelDidUpdateMsg{
		MsgType: EventDidCreateMsgType,
		UserID:  userID,
		EventID: eventID,
	}
}

func NewEventLocationDidUpdateMsg(userID string, eventID string) ModelDidUpdateMsg {
	return ModelDidUpdateMsg{
		MsgType: EventLocationDidUpdateMsgType,
		UserID:  userID,
		EventID: eventID,
	}
}

func NewEventScheduleDidUpdateMsg(userID string, eventID string) ModelDidUpdateMsg {
	return ModelDidUpdateMsg{
		MsgType: EventScheduleDidUpdateMsgType,
		UserID:  userID,
		EventID: eventID,
	}
}

func NewEventChatDidUpdateMsg(userID string, eventID string) ModelDidUpdateMsg {
	return ModelDidUpdateMsg{
		MsgType: EventChatDidUpdateMsgType,
		UserID:  userID,
		EventID: eventID,
	}
}

func UnmarshalModelUpdatedMessage(data []byte) (ModelDidUpdateMsg, error) {
	var msg ModelDidUpdateMsg
	err := json.Unmarshal(data, &msg)

	return msg, err
}

func (msg ModelDidUpdateMsg) Marshal() ([]byte, error) {
	return json.Marshal(msg)
}

// DirectMessage

type DirectMessage struct {
	Users []string          `json:"users"`
	Title string            `json:"title"`
	Text  string            `json:"text"`
	Image string            `json:"image"`
	Data  map[string]string `json:"data"`
}

func UnmarshalDirectMessage(data []byte) (DirectMessage, error) {
	var msg DirectMessage
	err := json.Unmarshal(data, &msg)

	return msg, err
}

func (msg DirectMessage) Marshal() ([]byte, error) {
	return json.Marshal(msg)
}
