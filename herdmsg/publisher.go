package herdmsg

import (
	"context"
	"io"

	"cloud.google.com/go/pubsub"
)

//go:generate mockery -name Publisher -outpkg herdmsgmocks -output ./herdmsgmocks -dir .
type Publisher interface {
	io.Closer

	PublishModelDidUpdate(context.Context, ModelDidUpdateMsg) (*pubsub.PublishResult, error)
	PublishDirectMessage(context.Context, DirectMessage) (*pubsub.PublishResult, error)
}

func NewPublisher(ctx context.Context, projectID string, topicID string) (Publisher, error)  {
	client, err := pubsub.NewClient(ctx, projectID)
	if err != nil {
		return nil, err
	}

	topic := client.Topic(topicID)

	return &publisherImpl{
		client:  client,
		topic:     topic,
	}, nil
}

// implementation

type publisherImpl struct {
	client  *pubsub.Client
	topic     *pubsub.Topic
}

func (p *publisherImpl) PublishModelDidUpdate(ctx context.Context, msg ModelDidUpdateMsg) (*pubsub.PublishResult, error) {
	data, err := msg.Marshal()
	if err != nil {
		return nil, err
	}

	pubsubMsg := &pubsub.Message{Data: data, Attributes: map[string]string{MsgTypeKey: MsgTypeModelDidUpdate}}
	res := p.topic.Publish(ctx, pubsubMsg)

	return res, nil
}

func (p *publisherImpl) PublishDirectMessage(ctx context.Context, msg DirectMessage) (*pubsub.PublishResult, error) {
	data, err := msg.Marshal()
	if err != nil {
		return nil, err
	}

	pubsubMsg := &pubsub.Message{Data: data, Attributes: map[string]string{MsgTypeKey: MsgTypeDirectMessage}}
	res := p.topic.Publish(ctx, pubsubMsg)

	return res, nil
}

func (p *publisherImpl) Close() error {
	if p.client != nil {
		return p.client.Close()
	}
	return nil
}