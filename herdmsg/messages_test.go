package herdmsg_test

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/herd-project/herd-messaging/herdmsg"
)

func TestModelDidUpdateMessage(t *testing.T) {
	tests := []struct {
		name string
		json string
		msg  herdmsg.ModelDidUpdateMsg
	}{
		{
			name: "profile did update",
			json: "{\"msg_type\": 1, \"user_id\": \"user-1\"}",
			msg:  herdmsg.NewProfileDidUpdateMsg("user-1"),
		},
		{
			name: "user did join event",
			json: "{\"msg_type\": 3, \"user_id\": \"user-1\", \"event_id\": \"event-1\"}",
			msg:  herdmsg.NewUserDidJoinEventMsg("user-1", "event-1"),
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := herdmsg.UnmarshalModelUpdatedMessage([]byte(tt.json))
			assert.NoError(t, err)
			assert.Equal(t, tt.msg, got)

			data, err := got.Marshal()
			assert.NoError(t, err)

			got2, err := herdmsg.UnmarshalModelUpdatedMessage(data)
			assert.NoError(t, err)
			assert.Equal(t, tt.msg, got2)
		})
	}
}

func TestDirectMessage(t *testing.T) {
	tests := []struct {
		msg herdmsg.DirectMessage
	}{
		{
			herdmsg.DirectMessage{
				Users: []string{"user-1"},
				Title: "title",
				Text:  "text",
				Image: "image",
				Data:  map[string]string{"key": "data"},
			},
		},
		{
			herdmsg.DirectMessage{
				Users: []string{"user-1", "user-2"},
				Title: "title",
				Text:  "text",
			},
		},
	}

	for _, tt := range tests {
		t.Run("", func(t *testing.T) {
			buf, err := tt.msg.Marshal()
			assert.NoError(t, err)

			m2, err := herdmsg.UnmarshalDirectMessage(buf)
			assert.NoError(t, err)

			assert.Equal(t, tt.msg, m2)
		})
	}
}