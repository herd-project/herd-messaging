module gitlab.com/herd-project/herd-messaging

require (
	cloud.google.com/go v0.49.0 // indirect
	cloud.google.com/go/pubsub v1.1.0
	github.com/stretchr/testify v1.2.2
	gitlab.com/herd-project/herd-shared v0.0.0-20191201081706-d9798ac0d48f
)

go 1.13
